/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import axios from "axios";
import {
  alfrescoFileService,
  alfrescoNodeService,
} from "@pristy/pristy-libvue";

class ActionsService {
  downloadFile(file) {
    let url = alfrescoFileService.getContentUrl(file.id);
    axios
      .get(url, { responseType: "blob" })
      .then((response) => {
        const blob = new Blob([response.data], {
          type: file.content.mimetype,
        });
        const link = document.createElement("a");
        link.href = URL.createObjectURL(blob);
        link.download = file.name;
        link.click();
        URL.revokeObjectURL(link.href);
      })
      .catch(console.error);
  }

  toggleDownloadFile(nodes) {
    if (!Array.isArray(nodes)) {
      this.downloadFile(nodes);
    } else if (nodes.length > 0) {
      nodes.forEach((file) => {
        this.downloadFile(file);
      });
    }
  }
  getRouteForNavigationInNewTab(workspaceId, item) {
    return {
      name: "navigation-dossier",
      params: {
        id: workspaceId,
        FolderId: item.id,
      },
    };
  }
  getRouteForOpenInNewTab(item) {
    let routeData;
    routeData = {
      name: "consultation-pdf",
      params: {
        id: item.id,
      },
    };
    return routeData;
  }

  getItemName(destId, item) {
    return alfrescoNodeService.getChildren(destId).then((data) => {
      let itemName = item.name;
      let destChildren = data.children;
      let initialItemName = item.isFile
        ? this.removeExtension(itemName)
        : itemName;

      let increment = destChildren.filter((child) =>
        child.name.startsWith(initialItemName),
      ).length;
      if (increment > 0) {
        itemName = item.isFile
          ? `${initialItemName}-${increment}.${this.getExtension(itemName)}`
          : `${initialItemName}-${increment}`;
      }
      return itemName;
    });
  }

  getExtension(fileName) {
    let initialFileName = fileName.split(".");
    return initialFileName[initialFileName.length - 1];
  }

  removeExtension(fileName) {
    return fileName.substring(0, fileName.lastIndexOf(".")) || fileName;
  }
}

export default new ActionsService();
