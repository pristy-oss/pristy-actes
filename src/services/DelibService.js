/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import {
  nodesApi,
  sitesApi,
  searchApi,
  alfrescoNodeService,
  useConfigStore,
} from "@pristy/pristy-libvue";

class ProductService {
  getWorkspaceContainers(siteId) {
    const opts = {
      skipCount: 0,
      maxItems: 1000,
    };

    return sitesApi
      .listSiteContainers(siteId, opts)
      .then((data) => {
        return data.list.entries.map((data) => data.entry);
      })
      .catch((error) => {
        let message;
        switch (error.status) {
          case 404:
            message =
              "Impossible de rechercher des séances sur un site inexistant.";
            break;
          default:
            message = error.message;
        }
        return message;
      });
  }

  searchWorkspaceSeances(siteId) {
    const query = {
      query: {
        language: "afts",
        userQuery: null,
        query: `SITE:"${siteId}" AND EXACTTYPE:"am:dossierSeance"`,
      },
      include: ["aspectNames", "properties", "allowableOperations"],
      sort: [{ type: "FIELD", field: "am:dateSeance", ascending: "false" }],
      paging: {
        maxItems: 1000,
        skipCount: 0,
      },
    };

    return searchApi.search(query);
  }

  searchDelibwith(value, espaceId) {
    let aftsQuery = `(TYPE:"am:documentActe" OR TYPE:"am:annexeConstitutive") AND TEXT:"${value}"`;
    if (espaceId !== undefined) {
      aftsQuery = `+SITE:${espaceId} AND ${aftsQuery}`;
    }

    const query = {
      query: {
        language: "afts",
        userQuery: null,
        query: aftsQuery,
      },
      include: ["aspectNames", "properties", "allowableOperations"],
      sort: [
        { type: "FIELD", field: "cm:name", ascending: "false" },
        { type: "FIELD", field: "am:datePublication", ascending: "false" },
        { type: "FIELD", field: "am:dateSeance", ascending: "false" },
      ],
      paging: {
        maxItems: 1000,
        skipCount: 0,
      },
    };

    return searchApi.search(query);
  }

  createFileOrFolder(nodeId, opts, type) {
    return nodesApi.createNode(nodeId, opts).catch((error) => {
      let message;
      switch (error.status) {
        case 400:
          message = "Les paramètres de la requête sont invalides.";
          break;
        case 403:
          message = `Vous n'avez pas les droits pour créer une ${type}.`;
          break;
        case 404:
          message = "Impossible de trouver le dossier parent.";
          break;
        case 409:
          message = `Une ${type} avec cette date est déjà existante.`;
          break;
        case 413:
          message =
            "Le contenu dépasse la limite de taille de fichier individuelle configurée pour le réseau ou le système";
          break;
        case 415:
          message = "Le type du contenu du noeud n’est pas supporté.";
          break;
        case 422:
          message =
            "Exception d’intégrité du modèle incluant un nom de fichier contenant des caractères non valides.";
          break;
        case 500:
          message = `Une ${type} avec cette date est déjà existante.`;
          break;
        case 507:
          message =
            "Le contenu dépasse la limite de quota de stockage globale configurée pour le réseau ou le système.";
          break;
        default:
          message = error.message;
      }
      throw new Error(message);
    });
  }

  getSeanceDelib(seanceId) {
    const opts = {
      skipCount: 0,
      maxItems: 1000,
      where: "(isFile=true)",
      include: ["aspectNames", "properties", "allowableOperations"],
    };

    return nodesApi.listNodeChildren(seanceId, opts);
  }

  memberSiteAccess(siteId, memberId) {
    const opts = {};

    return sitesApi.getSiteMembership(siteId, memberId, opts).then(
      (data) => {
        return data;
      },
      (error) => {
        let message;
        switch (error.status) {
          case 404:
            message = "Vous n’êtes pas membre du site.";
            break;
          default:
            message = error.message;
        }
        return message;
      },
    );
  }

  getAllActes(espaceId) {
    let query = `+TYPE:"am:documentActe"`;

    if (espaceId !== undefined) {
      query = `+SITE:${espaceId} AND ${query}`;
    }

    return this.queryActes(query);
  }

  getActesHorsSeance(espaceId, year) {
    let query = `+TYPE:"am:documentActe" AND -ASPECT:"am:dateSeance"`;
    if (year) {
      query += ` AND +am:dateActe:[${year}-01-01 TO ${year}-12-31]`;
    }
    if (espaceId !== undefined) {
      query = `+SITE:${espaceId} AND ${query}`;
    }
    return this.queryActes(query);
  }

  getActesSeance(searchSeance) {
    let query = `+PARENT:"${searchSeance}" AND +TYPE:"am:documentActe" AND ASPECT:"am:dateSeance"`;
    return this.queryActes(query);
  }

  queryActes(aftsQuery) {
    const query = {
      query: {
        language: "afts",
        userQuery: null,
        query: aftsQuery,
      },
      include: ["aspectNames", "properties", "allowableOperations"],
      sort: [{ type: "FIELD", field: "am:dateActe", ascending: "false" }],
      paging: {
        maxItems: 1000,
        skipCount: 0,
      },
    };

    return searchApi.search(query);
  }

  searchSeances(siteId) {
    const query = {
      query: {
        language: "lucene",
        userQuery: null,
        query: `+SITE:"${siteId}" +ASPECT:"am:dateSeance"`,
      },
      include: ["aspectNames", "properties", "allowableOperations"],
      paging: {
        maxItems: 1000,
        skipCount: 0,
      },
    };
    return searchApi.search(query).then((data) => {
      return data.list.entries.map((data) => data.entry);
    });
  }

  searchActes(siteId) {
    const query = {
      query: {
        language: "afts",
        userQuery: null,
        query: `SITE:"${siteId}" AND TYPE:"am:documentActe" AND NOT ASPECT:"am:dateSeance"`,
      },
      include: ["aspectNames", "properties", "allowableOperations"],
      paging: {
        maxItems: 1000,
        skipCount: 0,
      },
    };

    return searchApi.search(query);
  }

  getNode(id) {
    if (!id) {
      console.warn(`get node for '${id}' is invalide `);
      return;
    }
    const opts = {
      include: ["allowableOperations", "path", "relativePath"],
    };

    return nodesApi.getNode(id, opts);
  }

  modifyDelib(
    id,
    delibId,
    delibName,
    dateActe,
    delibDescription,
    delibTypeActe,
    delibCodeActe,
  ) {
    return nodesApi
      .updateNode(id, {
        name: delibId,
        properties: {
          "cm:title": delibName,
          "cm:description": delibDescription,
          "am:typeActe": delibTypeActe.type,
          "am:dateActe": dateActe
            ? new Date(
                Date.UTC(
                  dateActe.getFullYear(),
                  dateActe.getMonth(),
                  dateActe.getDate(),
                ),
              )
            : null,
          "am:codeActe": delibCodeActe,
        },
      })
      .then(
        (data) => {
          return data;
        },
        (error) => {
          let message;
          switch (error.status) {
            case 400:
            case 405:
              message = "Les paramètres de la requête sont invalides.";
              break;
            case 401:
              message = "L’authentification a échoué";
              break;
            case 403:
              message = "Vous n’avez pas les droits pour modifier l’élément.";
              break;
            case 404:
              message = "Impossible de trouver l’élément à modifier.";
              break;
            case 409:
              return {
                status: error.status,
                message: "Un élément avec ce nom est déjà existant.",
              };
            case 422:
              return {
                status: error.status,
                message:
                  "L’identifiant choisi ne respecte pas le format requis.",
              };
            default:
              message = error.message;
          }
          throw new Error(message);
        },
      );
  }

  updateDelibType(id, typeDelib) {
    const opts = {
      properties: {
        "am:typeActe": typeDelib,
      },
    };

    return nodesApi.updateNode(id, opts).then(
      (data) => {
        return data;
      },
      (error) => {
        let message;
        switch (error.status) {
          case 404:
            message = "Vous ne pouvez pas mettre à jour un fichier inexistant.";
            break;
          default:
            message = error.message;
        }
        throw new Error(message);
      },
    );
  }

  getSeanceWorkspace(seanceId) {
    return alfrescoNodeService.getParents(seanceId).then((yearFolder) => {
      return alfrescoNodeService.getParents(yearFolder.id).then((workspace) => {
        return workspace;
      });
    });
  }

  getActeAnnexes(delibId) {
    let annexes = [];
    return alfrescoNodeService.getChildren(delibId).then((children) => {
      children.children.forEach((child) => {
        if (child.nodeType === "am:annexeConstitutive") {
          annexes.push(child);
        }
      });
      return annexes;
    });
  }

  publishDelib(delibId) {
    const config = useConfigStore();
    let topicName = config.DELIB_PUBLICATION || "add-tampon-actes";
    let opts = { properties: { "pk:topic": topicName } };
    return nodesApi.updateNode(delibId, opts).then(
      (data) => {
        return data;
      },
      (error) => {
        let message;
        switch (error.status) {
          case 404:
            message = "Vous ne pouvez pas mettre à jour un fichier inexistant.";
            break;
          default:
            message = error.message;
        }
        throw new Error(message);
      },
    );
  }
}

export default new ProductService();
