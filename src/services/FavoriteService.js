/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { favoritesApi, useUserStore } from "@pristy/pristy-libvue";

class FavoriteService {
  getFavorites() {
    const user = useUserStore();

    const opts = {
      skipCount: 0,
      maxItems: 100,
      include: ["properties", "path"],
    };

    return favoritesApi
      .listFavorites(user.person.id, opts)
      .then((data) => {
        return data.list.entries.map((data) => data.entry);
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  }
}

export default new FavoriteService();
