/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { createRouter, createWebHistory } from "vue-router";
import {
  useUserStore,
  useConfigStore,
  alfrescoNodeService,
  useSelectionStore,
  useNavigationStore,
} from "@pristy/pristy-libvue";
import App from "./App.vue";
import PubPage from "./PubPage.vue";

const mimeTypeRouteMapping = {
  "application/pdf": "consultation-pdf",
};

const routes = [
  {
    path: "/",
    name: "app",
    component: App,
    children: [
      {
        path: "",
        name: "welcome",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./components/HelloWorld.vue"),
      },
      {
        path: "/admin/:id/:seance",
        name: "seance-actes",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./components/DelibPage.vue"),
      },
      {
        path: "/mes-espaces-actes",
        name: "mes-espaces-actes",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/EspaceActePage.vue"),
      },
      {
        path: "/mes-espaces-actes/:id/membres",
        name: "espace-actes-membres",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/EspaceActeMemberPage.vue"),
      },
      {
        path: "/admin/:id",
        name: "seances",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./components/SeancePage.vue"),
      },
      {
        path: "/pdf/:id",
        name: "consultation-pdf",
        meta: {
          requiresAuth: true,
          withBreadcrumb: true,
        },
        component: () => import("./pages/PdfPage.vue"),
      },

      {
        path: "/recherche",
        name: "recherche-actes",
        meta: {
          title: "Recherche | Pristy Actes administratifs",
          requiresAuth: true,
        },
        component: () => import("./components/RechercheDelib.vue"),
      },
      {
        path: "/:pathMatch(.*)*",
        name: "not-found-page",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/NotFoundPages.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () => import("./pages/LoginPage.vue"),
  },
  {
    path: "/interdit",
    name: "interdit",
    component: () => import("./pages/AccessDeniedPage.vue"),
  },
  {
    path: "/error/:id",
    name: "error",
    component: () => import("./pages/ErrorPage.vue"),
  },
  {
    path: "/consultation",
    redirect: { name: "consultation-public" },
  },
  {
    path: "/consultations",
    name: "consultation-public",
    component: () => import("./pages/PublicHomePage.vue"),
  },
  {
    path: "/consultation",
    name: "pub",
    component: PubPage,
    children: [
      {
        path: "/consultation/:espace_id",
        name: "pub-consultation-actes",
        meta: {
          publiclogin: true,
        },
        component: () => import("./pages/PublicDelibPage.vue"),
      },
      {
        path: "/consultation/:espace_id/pdf/:id",
        name: "pub-consultation-pdf",
        meta: {
          publiclogin: true,
          withBreadcrumb: false,
        },
        component: () => import("./pages/PdfPage.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory("/actes"),
  routes,
});

router.beforeEach(function (to, from, next) {
  window.scrollTo(0, 0);
  next();
});

router.beforeEach(function (to) {
  window.document.title =
    to.meta && to.meta.title ? to.meta.title : "Pristy Actes administratifs";
});

router.beforeEach(async (to, from) => {
  const user = useUserStore();
  if (to.matched.some((record) => record.meta.publiclogin)) {
    if (localStorage.getItem("ACS_USERNAME")) {
      await user.publiclogoff();
    }
    return user
      .publiclogin(to.params.espace_id)
      .then(() => {
        console.log("Login anonyme");
      })
      .catch((error) => {
        console.warn("Erreur d’authentification", error);
        router.push({
          name: "consultation-public",
        });
      });
  }

  if (to.matched.some((record) => record.meta.requiresAuth)) {
    await user.publiclogoff();
    const config = useConfigStore();

    if (config.AUTH === "keycloak" || config.AUTH === "lemonldap") {
      await user.connectKeycloak();
    } else {
      // Get Person to check if we are authenticate
      await user.getPerson();
      if (!user.isLoggedIn) {
        return { name: "login", query: { target: to.path } };
      }
    }
  }

  // Don't go to login if we are loggedIn
  if (user.isLoggedIn && from.name && to.name === "login") {
    return { name: "welcome" };
  }
});

router.beforeEach((to, from, next) => {
  const espacePattern = /^\/([a-f\d-]+)$/;
  const match = to.fullPath.match(espacePattern);
  let redirected = false;

  if (match && match.length > 1) {
    const nodeId = match[1];

    alfrescoNodeService
      .getNode(nodeId)
      .then((response) => {
        if (response.content && response.content.mimeType) {
          const mimeType = response.content.mimeType;

          for (const mimeTypePrefix in mimeTypeRouteMapping) {
            if (mimeType.startsWith(mimeTypePrefix)) {
              next({
                name: mimeTypeRouteMapping[mimeTypePrefix],
                params: { id: nodeId },
              });
              redirected = true;
              return;
            }
          }
        } else {
          if (response.isFolder) {
            let siteName = response.path.elements[2].name;
            next({
              name: "seance-actes",
              params: { id: siteName, seance: nodeId },
            });
            redirected = true;
            return;
          }
        }
        if (!redirected) {
          next();
        }
      })
      .catch((error) => {
        console.error("Erreur lors de la récupération du nœud:", error);
        next(false);
      });
  } else {
    next();
  }
});
router.beforeEach((to, from, next) => {
  const match = to.fullPath.match(/^\/site\/(.*)$/);
  if (match && match.length > 1) {
    const nodeId = match[1];
    next({
      name: "seances",
      params: { id: nodeId },
    });
  } else {
    next();
  }
});
router.beforeEach(async (to) => {
  await useSelectionStore().resetSelection();
  const navigationStore = useNavigationStore();
  if (to.name.startsWith("seance-actes")) {
    if (to.params.seance) {
      await navigationStore.loadOnActes(to.params.id, to.params.seance, false);
    }
  } else if (to.name.startsWith("seances")) {
    await navigationStore.loadOnActes(to.params.id, null, false);
  }
});

export default router;
