# Pristy Delib

Interface de gestion et de publication des actes : délibérations, arrêtés et décisions

Les utilisateurs administrateurs pourront gérer la dépose et la publication des actes:

* création des séances
* ajout de documents dans les séances

En mode visualisation, les utilisateurs pourront

* lister tous les actes
	* filtrer par date de séance
	* filtrer par type d'acte
* visualiser les documents

## Installation

1. Install all dependencies

```
npm install
```

2. Compiles project for development

```
npm run serve
```

3. Compiles and minifies for production

```
npm run build
```

4. Lints and fixes files

```
npm run lint
```

## Test

You can use pristy-demo to fully test this application. First start the demo stack, then start the development server.

``` bash
git clone pristy-demo
cd pristy-demo
./start.sh

cd ../pristy-actes
npm install
npm run serve
```

same with docker image

``` bash
docker run --rm -it --init --name=pristy-actes -v "$PWD:/usr/src/app" --net host --workdir=/usr/src/app docker.io/library/node:16.15.0 npm run serve
```

Note: Alfresco must be available on http://localhost:8080/alfresco to work.
Also `vue.config.js` and `public/env-config.json` must match.

> As some modules are present on our nexus, you need to add your credentials to your `.envrc` file


## Licensing

Copyright (C) 2022 - Jeci SARL - https://jeci.fr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
