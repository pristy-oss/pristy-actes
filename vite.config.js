import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { sentryVitePlugin } from "@sentry/vite-plugin";
import path from "path";

export default defineConfig({
  base: "/actes/",
  build: {
    sourcemap: false,
  },
  plugins: [
    vue(),
    sentryVitePlugin({
      org: "jeci-75336e1de",
      project: "pristy-actes",
      authToken: process.env.SENTRY_AUTH_TOKEN,
    }),
  ],
  optimizeDeps: {
    force: true,
  },
  resolve: {
    preserveSymlinks: true,
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
  server: {
    port: 8011,
    proxy: {
      "/alfresco": "http://localhost:8080",
    },
  },
});
