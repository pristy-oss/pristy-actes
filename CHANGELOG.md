# Changelog

1.8.0 / 2025-03-03
==================

- remove vue3-observe-visibility
- remove dsb-norge/vue-keycloak-js
- add vue-matomo
- Remove package intersection-observer and file visible.js
- FIX : logout on welcome page
- Add from when redirect to forgot password
- Add reset password link
- Correct publicLink action
- add select between csv files -> open data
- Prepare download csv open data

1.7.2 / 2024-11-06
==================

- remove disable edit codeActe

1.7.1 / 2024-06-03
==================

**Fixed bugs:**

- correct acte infos displayed if webdelib in public consultation
- Search acte by year
- Sort on Date Acte
- Prevent error 405
- max elements set to 1k
- Load theme in main.js

1.7.0 / 2024-04-27
==================

**New Features and Enhancements:**

- adapt logo css id for custom css
- Display error msg when update seance date
- Hide informationZone in public pdf consultation
- Remove upload new version if the file is already published
- Display reminder to upload files in fileupload
- Modify date and description of a seance
- Add Selection multiple
- Use themeStore
- Add redirection
- Change topbar logo (transparent)
- Display title then name in datatable
- Consultation - css card seance
- Refont css recherche delib
- add missing custom.css
- change get seance loading
- Update acte member page
- 3119: Menu action sur la page EspaceActe
- Add new BreadCrumb
- Add Sentry Configuration
- Add var BASE_URL
- Add ScrollTop on pdf page
- add pdf_viewer.css
- remove --alt-text-add-image

**Fixed bugs:**

- update vite - CVE-2024-23331
- Correct display when updating workspace
- Correct modifyNodePopup update file title
- Correct reload when updating seance date
- correct condition to enabled move button
- Correct Datakey + key datatable attribute for acteDatatable
- Correct actes workspace display in public consultation
- Correct pagination in acteDatatable
- Correct select All in datatable
- Correct use of navigationStore
- correct modifySeance and moveNodePopup
- Correct modify workspace after creation
- fix css popup import
- fix seance css
- Fix pdf page issues
- fix warning num rows
- fix DataView change from PrimeVue
- Fix #653 - Affichage des actes
- Correct bug e is undefined in listActe

**Update Dependencies**

- Replace vuecli by vite and update pdf page
- update package pristy-libvue 0.28.0
- Update Caddy to 2.7.6
- update node 20.9

1.6.0 / 2024-01-17
==================

- Fix modify dateActe
- Cas webdelib - Display title in place name
- Affiche titre si webdelib
- Revert affichage des années
- Add dateActe
- Affichage des infos webdelib
- CreateSeance - Date UTC
- Fix Rechercher consultation
- Fix CSS .header
- Fix affichage taille du fichier

1.5.0 / 2023-11-08
==================

- release script
- update pristy-libvue 0.16.7
- update pristy-libvue 0.16.6
- Add aspect isPublic
- add isPublic when creating and editing a workspace
- Correct bug actes consultation, correct searching + correct css
- Correct topbar layout + add button
- Hide séance are if there are no seances
- Correct dropdown typeActe filter
- Correct layout dataview header consultation view
- Reorganize les Actes area
- Sort search result on consultation
- ShareLink: force PDF Rendition
- Change default preview url
- Type prop to prevent error like
- ErrorPage
- need kaniko executor debug to has a shell
- Update .gitlab-ci.yml file
- build with kaniko
- build next branch
- init version 1.4.3
# Changelog

## [1.4.2](https://gitlab.com/pristy-oss/pristy-actes/-/compare/1.4.1...1.4.2) (2023-07-26)

**New Features and Enhancements:**

- Move Toast bottom-right
- Use Remixicon
- Better css design
- New icons for workplace visibility
- Remove our patched Image component

**Fixed bugs:**

- Ask to generate thumbnail if missing

**Update Dependencies**

- update vue-keycloak-js 2.4.0
- update core-js 3.31.1
- update pdfjs-dist 3.8.162
- update pinia 2.1.4
- update primeflex 3.3.1
- update primevue 3.30.0
- update remixicon 3.4.0
- update vue-router 4.2.4
- Update saga-green theme

## [1.4.1](https://gitlab.com/pristy-oss/pristy-actes/-/compare/1.4.0...1.4.1) (2023-05-22)

**New Features and Enhancements:**

- Update login page

**Fixed bugs:**

- Revert "canvasWrapper is not used" (slow loading of annexes pdf)
- Fix error dateSeance already exists

**Update Dependencies**

- update primevue 3.29.1
- update prdfjs 3.6.172
- update core-js 3.20.2
- update axios 1.4.0
- update remixicon 3.3.0
- update vue-router 4.2.1
- update pinia 2.1.3

## [1.4.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/1.3.1...1.4.0) (2023-05-10)

**New Features and Enhancements:**

- New public home page
- Add years, seances to public consultation page
- Add header on consultation pages
- Add research and css for logo image
- Refonte page de recherche
- intégration remixicon
- Use stageStorage from Dataview Component
- Add close button on ZoneInformation
- Rethink VuePdf with better toolbar
- Remove EventBus, not needed
- Remove mitt dependencies
- Add logo link to public actes consultation
- public actes consultation - change displayed actes by year without seance
- Add sort on workspaces in consultations page
- Amélioration de la topbar en vue mobile
- Améliroation de la page recherche vue mobile

**Fixed bugs:**

- Correction route si recherche
- use <router-link> instead of <a>
- the onImageLoadFailure can cause loop if fallback image is not loadable
- Fix load breadcrumb only when withBreadcrumb is true
- sort seance by date seance desc

**Update Dependencies**

- update @pristy/pristy-libvue 0.16.4
- update caddy 2.6.4

## [1.3.1](https://gitlab.com/pristy-oss/pristy-actes/-/compare/1.3.0...1.3.1) (2023-03-21)

**Fixed bugs:**

- changing the way we use services in templates
- A42 - The name field cannot be empty
- A3 - information button to the right of the ID field
- Topbar tooltip in bottom
- Simplification css

## [1.3.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/1.2.0...1.3.0) (2023-03-20)

**New Features and Enhancements:**

- PDF: correct computation au loading scale
- Gloable update of CSS design (align with pristy-espaces)
- add detail and version view
- use config store (remove window.config usage)
- Add favorites to MenuStore
- Add share link popup
- Add Keycloak logout
- In member page, differentiating users from groups
- Add zoneInformation Component

**Fixed bugs:**

- Fix updating for the public consultation of the acts
- Fix "response.entry" is undefined when importing
- bug loadThumbnail
- Removable sort in datatable
- Fix display of upload errors
- Correction on login page
- In breadcrumb path, display title of workspace instead of id
- add redirect on errors without status

**Update Dependencies**

- update pdfjs 3.4.120
- update @pristy/pristy-libvue 0.13.3
- update core-je 3.29.1
- update jquery 3.6.4
- update pinia 2.0.33
- update primevue 3.25.0
- update @dsb-norge/vue-keycloak-js 2.2.0
- update axios 1.3.4

## [1.2.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/1.1.0...1.2.0) (2023-01-31)

**New Features and Enhancements:**

- Rework login page
- Improve breadcrumb for Seance, Delib and Pdf pages

**Update Dependencies**

- Use @pristy/pristy-libvue 0.9.0


## [1.1.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/1.0.0...1.1.0) (2023-01-25)

**New Features and Enhancements:**

- Add error page when Alfresco is not started
- Add keycloak-js to permit OIDC authentication
- Add group management functions

**Fixed bugs:**

- Fix datepicker and dropdown bugs
- Fix import acte bug

**Update Dependencies**

- Use @pristy/pristy-libvue 0.8.1
- Update primevue to 3.22.3
- Update axios to 1.2.3
- Update vue to 3.2.45

## [1.0.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/0.10.0...1.0.0) (2022-12-22)

**New Features and Enhancements:**

- Conserver nombre fichiers par page dans datatable
- Conserver le nombre d’éléments par page dans les seances
- label de vignette dans fenetre d'upload
- Change upload limit to 200Mo
- Display icones si la preview n’est pas affichable
- configuration présence webdelib
- masquage des champs inutiles
- Ajout champs webdelib
- Ajout fichier json pour codes actes
- Ajout de la corbeille et la liste des espaces actes au menu options
- Déplacer le bouton créer
- Gérer l'affichage mobile du bouton
- Sticky toolbar sur la consultation des pdf
- Masquer zone séance en fonction d'un boolean

**Fixed bugs:**

- Menu download des annexes ne s’affiche plus
- correction du bug de recherche en vue consultation
- correction du fonctionnement de la page de recherche sur l’input vide
- fonctionnement avec une url en entrée
- Remise en etat du en cours de publication
- bug sur l'erreur 409

**Update Dependencies**

- Update @pristy/pristy-libvue 0.6.2
- Update primevue to 3.21.0
- Update jquery to 3.6.3

## [0.8.1](https://gitlab.com/pristy-oss/pristy-actes/-/compare/0.8.0...0.8.1) (2022-11-23)

**Fixed bugs:**

- Mettre à jour liste actes hors séance à la navigation entre espace   


## [0.8.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/0.7.0...0.8.0) (2022-11-21)

**New Features and Enhancements:**

- Afficher si les actes ont des annexes
- Tri colonne publié le
- Menu éléments tronqués si trop longs
- Ajout page liste espace acte
- Ajout titre de page dans routeur 
 
**Fixed bugs:**

- Fix breadcrumb pour les actes hors séances
- Afficher bouton "créer séance" que si l'on a les droits
- Afficher bouton "importer séance" que si l'on a les droits
- Correction bug modification espace après création
- Correction id espaceActe à la création pour favoris
- Corriger créer et ajouter des membres

**Update Dependencies**

- Update primevue 3.18.1
- Update @pristy/pristy-libvue 0.4.1      

## [0.7.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/0.6.0...0.7.0) (2022-11-04)

**New Features and Enhancements:**

- Affichage du numéro de version du document 
- Branchement barre de recherche 


## [0.6.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/0.5.0...0.6.0) (2022-10-25)

**New Features and Enhancements:**

- Amelioration parcours utilisateur
- Evolution du modele de données sur pristy-core 1.1.0
- Integration de l’action publication (necessite pristy-kafka 0.1.0)
- Affichage des actes qui ne sont pas dans des séances

**Update Dependencies**

- Use pristy-libevue 0.3.1


## [0.5.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/0.4.1...0.5.0) (2022-10-12)

**New Features and Enhancements:**

- New dynamic menu
- Ajout de public login/log off dans le router
- Popup d’import des actes
- Enhance column sizes for fileupload component
- Ajout bouton et colonne liés à la publication
- Suppression du bouton ‘importer des actes’ dans la zone d’action
- Zone d’action version mobile  
- Changement texte page accueil
- Mise à jour dynamique de la vue d’un espace par les liens internes to
- Ajout du lien vers le portail dans le menu général

**Fixed bugs:**

- seancePage update user store ref
- Correction redirection recherche actes menu-development     
- Problème d’espacement de colonne en vue réduite        

**Update Dependencies**

- Use pristy-libevue 0.2 


## [0.4.1](https://gitlab.com/pristy-oss/pristy-actes/-/compare/0.4.0...0.4.1) (2022-08-31)

**Fixed bugs:**

- Correction d’un problème de routage 


## [0.4.0](https://gitlab.com/pristy-oss/pristy-actes/-/compare/0.3.0...0.4.0) (2022-08-30)

**New Features and Enhancements:**

- Intégration de pristy-libvue

**Fixed bugs:**

- fix breadcrumb : nom de l’espace au lieu de l’id


## [0.3.0](https://gitlab.beezim.fr/jeci/pristy/pristy-actes/-/tags/0.3.0) (2022-07-13)
Première version publique
